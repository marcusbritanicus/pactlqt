TEMPLATE = app
TARGET = volume

QT += widgets

VERSION = 2.7.0

# Library section
unix:!macx: LIBS += -lcprime

# Disable warnings, enable threading support and c++11
CONFIG  += thread silent build_all c++11

# Build location
BUILD_PREFIX = $$(CA_BUILD_DIR)
isEmpty( BUILD_PREFIX ) {
	BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-plugins/
OBJECTS_DIR   = $$BUILD_PREFIX/obj-plugins/
RCC_DIR       = $$BUILD_PREFIX/qrc-plugins/
UI_DIR        = $$BUILD_PREFIX/uic-plugins/

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        INSTALLS += target

        target.path = $$PREFIX/lib/coreapps/plugins
}

HEADERS += pactlqt.hpp
SOURCES += pactlqt.cpp main.cpp
